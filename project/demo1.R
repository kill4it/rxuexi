# 导入所需的包
library(shiny)
library(DBI)
library(RMySQL)
library(ggplot2)
library(pheatmap)
library(DT)
# 连接到MySQL数据库
con <- dbConnect(RMySQL::MySQL(),
 dbname = "test",
 host = "106.55.191.228",
 port = 3306,
 user = "root",
 password = "Luopeijie123."
)

# 创建Shiny应用程序界面
# 创建Shiny应用程序界面
ui <- navbarPage(
  "生物菌落数据管理系统",
  tabPanel(
    "数据管理",
    fluidPage(
      fluidRow(
        column(
          4,
          wellPanel(
            h3("菌落数据录入"),
            textInput("size", "菌落大小"),
            textInput("shape", "菌落形状"),
            textInput("color", "菌落颜色"),
            actionButton("addBtn", "添加数据")
          ),
          wellPanel(
            h3("菌落数据查询"),
            textInput("querySize", "查询菌落大小"),
            actionButton("queryBtn", "查询数据")
          ),
          wellPanel(
            h3("菌落数据删除"),
            textInput("deleteID", "要删除的菌落ID"),
            actionButton("deleteBtn", "删除数据")
          ),
          wellPanel(
            h3("菌落数据修改"),
            textInput("modifyID", "要修改的菌落ID"),
            textInput("newSize", "新的菌落大小"),
            textInput("newShape", "新的菌落形状"),
            textInput("newColor", "新的菌落颜色"),
            actionButton("modifyBtn", "修改数据")
          )
        ),
        column(
          8,
          dataTableOutput("queryResult")
        )
      )
    )
  ),
  tabPanel(
    "数据可视化",
    fluidPage(
      fluidRow(
        column(
          4,
          wellPanel(
            h3("可视化选项"),
            radioButtons("plotType", "选择可视化类型：",
              choices = c("火山图", "热图", "雷达图", "气泡图")
            ),
            actionButton("plotBtn", "生成可视化")
          )
        ),
        column(
          8,
          plotOutput("plot")
        )
      )
    )
  )
)

# 创建服务器逻辑
server <- function(input, output, session) {
  # 添加数据到数据库
  observeEvent(input$addBtn, {
    query <- "INSERT INTO biocolony_data (size, shape, color) VALUES ("++", ?, ?)"
    statement <- dbSendStatement(con, query)
    dbBind(statement, list(input$size, input$shape, input$color))
    dbClearResult(statement)
  })

  # 查询数据
  output$queryResult <- renderDataTable({
    # 如果查询条件为空，则返回所有数据；否则按照查询条件过滤数据
    if (is.null(input$querySize) || input$querySize == "") {
      query <- "SELECT * FROM biocolony_data"
    } else {
      query <- paste("SELECT * FROM biocolony_data WHERE size = '", input$querySize, "'", sep = "")
    }
    result <- dbGetQuery(con, query)
    result
  })

  # 删除数据
  observeEvent(input$deleteBtn, {
    query <- paste("DELETE FROM biocolony_data WHERE id = ", input$deleteID, sep = "")
    dbExecute(con, query)
  })

  # 修改数据
  observeEvent(input$modifyBtn, {
    query <- paste("UPDATE biocolony_data SET size = '", input$newSize,
      "', shape = '", input$newShape,
      "', color = '", input$newColor,
      "' WHERE id = ", input$modifyID,
      sep = ""
    )
    dbExecute(con, query)
  })

  # 生成可视化图表
  output$plot <- renderPlot({
    # 获取数据库中的数据
    data <- dbGetQuery(con, "SELECT * FROM biocolony_data")

    # 根据用户选择生成火山图或热图
    if (input$plotType == "火山图") {
      # 计算差异表达和显著性（示例：随机生成）
      data$expression <- rnorm(nrow(data), mean = 0, sd = 1)
      data$p_value <- runif(nrow(data), min = 0, max = 0.05)

      # 根据显著性标识差异显著的数据点
      data$significant <- ifelse(data$p_value < 0.05, "Yes", "No")

      # 创建火山图
      ggplot(data, aes_string(x = "size", y = "expression", color = "significant")) +
        geom_point() +
        scale_color_manual(values = c("red", "black"), guide = FALSE) +
        labs(x = "Size", y = "Expression", title = "Volcano Plot")
    } else if (input$plotType == "热图") {
      # 将 size, shape 和 color 转换为数值（示例）
      data_matrix <- as.matrix(data.frame(
        size = as.numeric(as.factor(data$size)),
        shape = as.numeric(as.factor(data$shape)),
        color = as.numeric(as.factor(data$color))
      ))
      rownames(data_matrix) <- data$id

      # 创建热图
      pheatmap(data_matrix)
    }
  })

  # 在会话结束时关闭数据库连接
 # session$onSessionEnded(function() {
  #  dbDisconnect(con)
  #  cat("数据库连接已关闭")
  #})

  observeEvent(input$closeBtn, {
    stopApp()
  })

}

# 启动Shiny应用程序
shinyApp(ui = ui, server = server)
# shiny::runApp("/Users/luopeijie/Rfile/test/index.R")
